﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactManagment.Models
{
    public class RegistrationModels
    {
        public int SrNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
    }
}