﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ContactManagment.Models;

namespace ContactManagment.DataAcess
{
    public class RegistrationOperation
    {
        public static string Conn = ConfigurationManager.ConnectionStrings["mycon"].ToString();
        #region Crud Operation
        public string SaveData(RegistrationModels objRegistrationModels)
        {
            int result = 0;
            try
            {
                var objcon = new SqlConnection(Conn);
                SqlCommand cmd = new SqlCommand("Usp_InsertUpdateDelete_Customer", objcon);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@CustomerID", 0);    
                cmd.Parameters.AddWithValue("@FirstName", objRegistrationModels.FirstName);
                cmd.Parameters.AddWithValue("@LastName", objRegistrationModels.LastName);
                cmd.Parameters.AddWithValue("@DateOfBirth", objRegistrationModels.DateOfBirth.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Gender", objRegistrationModels.Gender);
                cmd.Parameters.AddWithValue("@Operation", "I");
                objcon.Open();
                result = cmd.ExecuteNonQuery();
                objcon.Close();
            }
            catch (Exception ex)
            {

            }
            return result.ToString();
        }
        public string EditData(RegistrationModels objRegistrationModels)
        {
            int result = 0;
            try
            {
                var objcon = new SqlConnection(Conn);
                SqlCommand cmd = new SqlCommand("Usp_InsertUpdateDelete_Customer", objcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SrNo", objRegistrationModels.SrNo);
                cmd.Parameters.AddWithValue("@FirstName", objRegistrationModels.FirstName);
                cmd.Parameters.AddWithValue("@LastName", objRegistrationModels.LastName);
                cmd.Parameters.AddWithValue("@DateOfBirth", objRegistrationModels.DateOfBirth.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Gender", objRegistrationModels.Gender);
                cmd.Parameters.AddWithValue("@Operation", "E");
                objcon.Open();
                result = cmd.ExecuteNonQuery();
                objcon.Close();
            }
            catch (Exception ex)
            {

            }
            return result.ToString();
        }
        #endregion
    }
}