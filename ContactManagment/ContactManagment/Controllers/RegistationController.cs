﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactManagment.Controllers
{
    public class RegistationController : ApiController
    {
        // GET: api/Registation
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Registation/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Registation
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Registation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Registation/5
        public void Delete(int id)
        {
        }
    }
}
