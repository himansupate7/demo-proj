﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactManagment.Models;
using ContactManagment.DataAcess;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ContactManagment.Controllers
{
    public class RegistrationController : Controller
    {
        public static string Conn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        public RegistrationController()
        {

        }
        // GET: Registration
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ShowAllDetails()
        {
            var a = GetAllDetails(0);
            return View(a);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var b = GetEditDetails(id);
            return View(b);

        }
        [HttpPost]
        public ActionResult Edit(RegistrationModels _RegistrationModels)
        {
            var b = EditData(_RegistrationModels);
            return RedirectToAction("ShowAllDetails");
        }
        public RegistrationModels GetEditDetails(int Srno)
        {
            var obj = new RegistrationModels();

            var objcon = new SqlConnection(Conn);
            SqlCommand cmd = new SqlCommand("Usp_GetData", objcon);
            cmd.Parameters.AddWithValue("@SrNO", Srno);
            cmd.CommandType = CommandType.StoredProcedure;
            objcon.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable();
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                obj.SrNo = Convert.ToInt32(dr["SrNo"]);
                obj.FirstName = dr["FirstName"].ToString();
                obj.LastName = dr["LastName"].ToString();
                obj.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"].ToString());
                obj.Gender = dr["Gender"].ToString() == "M" ? "Male" : "Female";
            }
            objcon.Close();
            return obj;
        }
        public List<RegistrationModels> GetAllDetails(int Srno)
        {
            var objList = new List<RegistrationModels>();

            var objcon = new SqlConnection(Conn);
            SqlCommand cmd = new SqlCommand("Usp_GetData", objcon);
            cmd.Parameters.AddWithValue("@SrNO", Srno);
            cmd.CommandType = CommandType.StoredProcedure;
            objcon.Open();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataTable dt = new DataTable();
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                var obj = new RegistrationModels();
                obj.SrNo = Convert.ToInt32(dr["SrNo"]);
                obj.FirstName = dr["FirstName"].ToString();
                obj.LastName = dr["LastName"].ToString();
                obj.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"].ToString());
                obj.Gender = dr["Gender"].ToString() == "M" ? "Male" : "Female";
                objList.Add(obj);
            }
            objcon.Close();
            return objList;
        }
        public JsonResult InsertRegistration(RegistrationModels _Registration)
        {
            string Ischecked = "";
            Ischecked = SaveData(_Registration);
            return Json(Ischecked);
        }
        #region Crud Operation
        public string SaveData(RegistrationModels objRegistrationModels)
        {
            int result = 0;
            try
            {
                var objcon = new SqlConnection(Conn);
                SqlCommand cmd = new SqlCommand("Usp_ContactMan_Crud", objcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FirstName", objRegistrationModels.FirstName);
                cmd.Parameters.AddWithValue("@LastName", objRegistrationModels.LastName);
                cmd.Parameters.AddWithValue("@DateOfBirth", objRegistrationModels.DateOfBirth.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Gender", objRegistrationModels.Gender == "M" ? "Male" : "Female");
                cmd.Parameters.AddWithValue("@Operation", "I");
                objcon.Open();
                result = cmd.ExecuteNonQuery();
                objcon.Close();
            }
            catch (Exception ex)
            {

            }
            return result.ToString();
        }
        public string EditData(RegistrationModels objRegistrationModels)
        {
            int result = 0;
            try
            {
                var objcon = new SqlConnection(Conn);
                SqlCommand cmd = new SqlCommand("Usp_ContactMan_Crud", objcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SrNo", objRegistrationModels.SrNo);
                cmd.Parameters.AddWithValue("@FirstName", objRegistrationModels.FirstName);
                cmd.Parameters.AddWithValue("@LastName", objRegistrationModels.LastName);
                cmd.Parameters.AddWithValue("@DateOfBirth", objRegistrationModels.DateOfBirth.ToString("yyyy-MM-dd"));
                cmd.Parameters.AddWithValue("@Gender", objRegistrationModels.Gender);
                cmd.Parameters.AddWithValue("@Operation", "E");
                objcon.Open();
                result = cmd.ExecuteNonQuery();
                objcon.Close();
            }
            catch (Exception ex)
            {

            }
            return result.ToString();
        }
        #endregion
    }
}