﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ContactManagment.Startup))]
namespace ContactManagment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
